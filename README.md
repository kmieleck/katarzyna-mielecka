# Katarzyna Mielęcka
## Portfolio of Projects
This repository contains a selection of projects I have worked on, both individually and collaboratively. Projects are organized into separate branches within the repository for clarity and organization. Feel free to explore the variety of projects showcased here, ranging from individual endeavors to team collaborations.
